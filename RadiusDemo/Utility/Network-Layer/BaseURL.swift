//
//  BaseURL.swift
//  RadiusDemo
//
//  Created by Shailendra Kumar Gupta on 29/06/23.
//

import Foundation

struct BaseURL {

    static let kDevURL    = "https://my-json-server.typicode.com/"
}
