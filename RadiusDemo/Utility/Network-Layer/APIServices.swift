//
//  APIServices.swift
//  RadiusDemo
//
//  Created by Shailendra Kumar Gupta on 29/06/23.


import Foundation
import UIKit


class APIServices{
    
    static let shared = APIServices()
   
    init(){}
    
    func loadData(url: String,method : HTTPMethod = .GET,param : [String: Any]?, success:@escaping (Data) -> (),failure:@escaping (String) -> ()) {
        let session = URLSession.shared
        var request = URLRequest(url: URL(string:BaseURL.kDevURL + url)!)
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        if param != nil {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param as Any, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
                return
            }
        }
        let task = session.dataTask(with: request) { data, response, error in
            if let error = error {
                print("Post Request Error: \(error.localizedDescription)")
                return
            }
            let httpResponse = response as? HTTPURLResponse
            if (200...299).contains(httpResponse!.statusCode) {
                
            }else if (400...500).contains(httpResponse!.statusCode){
                failure("Something went wrong please try again!!")
                return
            }
            
            guard let responseData = data else {
                print("nil Data received from the server")
                return
            }
            
            do {
                if let jsonResponse = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? [String: Any] {
                    success(responseData)
                } else {
                    print("data maybe corrupted or in wrong format")
                    throw URLError(.badServerResponse)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
}
