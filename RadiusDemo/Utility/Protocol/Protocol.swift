//
//  protocol.swift
//  RadiusDemo
//
//  Created by Shailendra Kumar Gupta on 29/06/23.
//

import Foundation

protocol ExclusionsDelegate{
    func exclusionsDelegate(_ exclusions:[String:String])
}
