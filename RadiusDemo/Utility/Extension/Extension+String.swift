//
//  Extension+Swift.swift
//  RadiusDemo
//
//  Created by Shailendra Kumar Gupta on 29/06/23.
//

import Foundation
import SwiftUI

extension String {
    
    static let kEmpty         = ""
    static let kOneSpace      = " "
    static let kOK            = "OK"
}
