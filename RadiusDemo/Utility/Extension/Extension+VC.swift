//
//  Extension+VC.swift
//  RadiusDemo
//
//  Created by Shailendra Kumar Gupta on 29/06/23.
//

import Foundation
import UIKit

extension ViewController {
    
    func showAlert(title:String,message:String) {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: .kOK, style: UIAlertAction.Style.destructive, handler: { _ in }))
        DispatchQueue.main.async {
            self.present(alert, animated: false, completion: nil)
        }
    }
}
