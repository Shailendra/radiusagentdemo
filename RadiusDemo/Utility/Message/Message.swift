//
//  Message.swift
//  RadiusDemo
//
//  Created by Shailendra Kumar Gupta on 29/06/23.
//

import Foundation

struct Message {
    
    static let InternetConnection = "Please Check Your Internet Connection"
    static let YouCantSelect      = "You can't select this option"
}
