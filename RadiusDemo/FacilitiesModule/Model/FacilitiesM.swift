//
//  FacilitiesM.swift
//  RadiusDemo
//
//  Created by Shailendra Kumar Gupta on 29/06/23.
//

import Foundation

struct FacilitiesCodable : Codable {
    
    let facilities : [Facilities]?
    let exclusions : [[Exclusions]]?
    
    enum CodingKeys: String, CodingKey {
        
        case facilities = "facilities"
        case exclusions = "exclusions"
    }
    
    init(from decoder: Decoder) throws {
        
        let values      = try decoder.container(keyedBy: CodingKeys.self)
        self.facilities = try values.decodeIfPresent([Facilities].self, forKey: .facilities)
        self.exclusions = try values.decodeIfPresent([[Exclusions]].self, forKey: .exclusions)
    }
    
}

struct Exclusions : Codable {
   
    let facility_id : String?
    let options_id  : String?
    
    enum CodingKeys: String, CodingKey {
        
        case facility_id = "facility_id"
        case options_id  = "options_id"
    }
    
    init(from decoder: Decoder) throws {
        
        let values       = try decoder.container(keyedBy: CodingKeys.self)
        self.facility_id = try values.decodeIfPresent(String.self, forKey: .facility_id)
        self.options_id  = try values.decodeIfPresent(String.self, forKey: .options_id)
    }
}

struct Facilities : Codable {
    
    let facility_id : String?
    let name        : String?
    let options     : [Options]?
    
    enum CodingKeys: String, CodingKey {
        
        case facility_id = "facility_id"
        case name        = "name"
        case options     = "options"
    }
    
    init(from decoder: Decoder) throws {
        let values       = try decoder.container(keyedBy: CodingKeys.self)
        self.facility_id = try values.decodeIfPresent(String.self, forKey: .facility_id)
        self.name        = try values.decodeIfPresent(String.self, forKey: .name)
        self.options     = try values.decodeIfPresent([Options].self, forKey: .options)
    }
}

struct Options : Codable {
    
    let name : String?
    let icon : String?
    let id   : String?
    
    enum CodingKeys: String, CodingKey {
        
        case name = "name"
        case icon = "icon"
        case id   = "id"
    }
    
    init(from decoder: Decoder) throws {
       
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.name  = try values.decodeIfPresent(String.self, forKey: .name)
        self.icon  = try values.decodeIfPresent(String.self, forKey: .icon)
        self.id    = try values.decodeIfPresent(String.self, forKey: .id)
    }
}
