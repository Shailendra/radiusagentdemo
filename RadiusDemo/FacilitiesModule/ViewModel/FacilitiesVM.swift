//
//  FacilitiesVM.swift
//  RadiusDemo
//
//  Created by Shailendra Kumar Gupta on 29/06/23.
//

import Foundation

class FacilitiesVM : ObservableObject {
    
    //MARK: - PROPERTIES
    var facilitiesData : FacilitiesCodable!
    
    func CallWebAPI(completion : @escaping (String,Bool) -> (),failure:@escaping (String) -> ()){
        APIServices.shared.loadData(url: EndPointURL.assignment,method: .GET, param: nil) { [self] data in
            do {
                self.facilitiesData = try JSONDecoder().decode(FacilitiesCodable.self, from: data)
                completion(.kEmpty, true)
            } catch {
                print(error)
            }
        }  failure: { error in
            failure(error.debugDescription)
        }
    }
}
