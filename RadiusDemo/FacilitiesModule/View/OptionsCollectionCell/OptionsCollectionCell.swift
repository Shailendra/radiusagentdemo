//
//  OptionsCollectionCell.swift
//  RadiusDemo
//
//  Created by Shailendra Kumar Gupta on 29/06/23.
//

import UIKit

class OptionsCollectionCell: UICollectionViewCell {

    //MARK: - PROPERTIES
    @IBOutlet weak var optionNameLabel : UILabel!
    @IBOutlet weak var imageView       : UIImageView!
    @IBOutlet weak var bgView          : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
