//
//  OptionsCollectionView.swift
//  RadiusDemo
//
//  Created by Shailendra Kumar Gupta on 29/06/23.
//

import Foundation
import UIKit

extension FacilitiesCell : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.options != nil ? self.options?.count ?? 0 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell( withReuseIdentifier: IdentifierName.Options, for: indexPath) as! OptionsCollectionCell
        
        if self.options != nil{
            cell.optionNameLabel.text = "\(self.options?[indexPath.item].name ?? .kEmpty)"
            cell.imageView.image = UIImage(named: self.options?[indexPath.item].icon ?? .kEmpty)
            cell.bgView.layer.cornerRadius = self.index == indexPath.item ? 4 : 0
            cell.bgView.layer.borderWidth  = self.index == indexPath.item ? 1 : 0
            cell.bgView.layer.borderColor  = self.index == indexPath.item ? UIColor.green.cgColor : UIColor.clear.cgColor
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.exclusionsDelegate(["facility_id":self.facility_id ,"options_id":self.options?[indexPath.item].id ?? .kEmpty])
        self.index = indexPath.item
        self.collectionView.reloadData()
    }
}
