//
//  FacilitiesCell.swift
//  RadiusDemo
//
//  Created by Shailendra Kumar Gupta on 29/06/23.
//

import UIKit

class FacilitiesCell: UITableViewCell {

    //MARK: - PROPERTIES
    @IBOutlet weak var bgView         : UIView!
    @IBOutlet weak var nameLabel      : UILabel!
    @IBOutlet weak var collectionView : UICollectionView!
    var facility_id                   : String          = .kEmpty
    var options                       : [Options]?      = nil
    var exclusions                    : [[Exclusions]]? = nil
    var index                         : Int             = -1
    var exclusionsArray               = [[String : String]]()
    var delegate                      : ExclusionsDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.registorCellAndNib()
        self.bgView.dropShadow()
    }
    
    func registorCellAndNib(){
        self.collectionView.register(OptionsCollectionCell.self, forCellWithReuseIdentifier: IdentifierName.Options)
        self.collectionView.register(UINib(nibName:IdentifierName.Options, bundle: nil), forCellWithReuseIdentifier:IdentifierName.Options)
    }
}
