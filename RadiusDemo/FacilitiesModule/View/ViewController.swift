//
//  ViewController.swift
//  RadiusDemo
//
//  Created by Shailendra Kumar Gupta on 29/06/23.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: - PROPERTIES
    @IBOutlet weak var facilitiesTableView : UITableView!
    var vm                                 = FacilitiesVM()
    var exclusionsArray                    = [[String : String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.facilitiesTableView.register(UINib(nibName: IdentifierName.Facilities, bundle: nil), forCellReuseIdentifier: IdentifierName.Facilities)
        self.FacilitiesAPICall()
    }
    
    //MARK: - Facilities API CALLING
    fileprivate func FacilitiesAPICall() {
        if Reachability.isConnectedToNetwork(){
            self.vm.CallWebAPI() { msg,success  in
                DispatchQueue.main.async {
                    self.facilitiesTableView.reloadData()
                }
            } failure: { error in
                self.showAlert(title: .kEmpty, message: error)
            }
        }else{
            self.showAlert(title: .kEmpty, message: Message.InternetConnection)
        }
    }
    
    @IBAction func resetAction(){
        self.exclusionsArray.removeAll()
        self.facilitiesTableView.reloadData()
    }
}

