//
//  FacilitiesTableViewDataSource.swift
//  RadiusDemo
//
//  Created by Shailendra Kumar Gupta on 29/06/23.
//

import Foundation
import UIKit

extension ViewController : UITableViewDelegate, UITableViewDataSource,ExclusionsDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.vm.facilitiesData == nil ? 0 : self.vm.facilitiesData.facilities!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: IdentifierName.Facilities, for: indexPath) as! FacilitiesCell
        if self.vm.facilitiesData != nil{
            cell.nameLabel.text = self.vm.facilitiesData.facilities?[indexPath.row].name ?? .kEmpty
            cell.options        = self.vm.facilitiesData.facilities?[indexPath.row].options ?? []
            cell.exclusions     = self.vm.facilitiesData.exclusions ?? [[]]
            cell.facility_id    = self.vm.facilitiesData.facilities?[indexPath.row].facility_id ?? .kEmpty
            cell.delegate       = self
            cell.index          = -1
            cell.collectionView.reloadData()
        }
        return cell
    }
    func exclusionsDelegate(_ exclusions: [String : String]) {
        
        if self.exclusionsArray.count == 0 {
            self.exclusionsArray.append(exclusions)
        }else if self.exclusionsArray.count == 1 {
            if self.exclusionsArray.contains(exclusions){
                self.exclusionsArray.removeAll(where: { $0 == exclusions})
            }
            self.exclusionsArray.append(exclusions)
            self.itrationOfExclusionsArray()
            
        }else{
            self.exclusionsArray.remove(at: 1)
            self.exclusionsArray.append(exclusions)
            self.itrationOfExclusionsArray()
        }
    }
    
    func itrationOfExclusionsArray(){
        for i in 0..<(self.vm.facilitiesData.exclusions?.count ?? 0){
            let exclusions11 = self.vm.facilitiesData.exclusions?[i] ?? []
            var exclusionsArray1 = [[String : String]]()
            for j in 0..<(exclusions11.count) {
                exclusionsArray1.append(["facility_id":exclusions11[j].facility_id ?? .kEmpty ,"options_id":exclusions11[j].options_id ?? .kEmpty])
                if exclusionsArray1 == self.exclusionsArray {
                    self.showAlert(title: .kEmpty, message: Message.YouCantSelect)
                    self.exclusionsArray.removeAll()
                    self.facilitiesTableView.reloadData()
                    break
                }
            }
        }
    }
}
